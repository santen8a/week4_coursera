<?php

function displayAllWork(){
	?>
	<div id="allWorkList">

		<div class="row">
			<table class="table table-hover">
				<thead>
					<th>ProjectID</th>
					<th>Name</th>
					<th>Start Date</th>
					<th>Duration</th>
					<th>End Date</th>
					<th>Location</th>
				</thead>
				<tbody>
					
					<?php
					$list = get_all_work_details();
					for($i=0;$i<sizeof($list);$i++){
						?>
						<tr>
							<td><?php echo $list[$i]['projectID']; ?></td>
							<td><a href='view_work.php?id=<?php echo $list[$i]['projectID']; ?>'><?php echo $list[$i]['name']; ?></a></td>
							<td><?php echo $list[$i]['startDate']; ?></td>
							<td><?php echo $list[$i]['duration']; ?></td>
							<td><?php echo $list[$i]['endDate']; ?></td>
							<td><?php echo $list[$i]['Location']; ?></td>
						</tr>

						<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>


	<?php
}
function display_project_basic_info($projectID)
{
	?>
	<table class="table table-hover">
		<thead>
			<th>ProjectID</th>
			<th>Name</th>
			<th>Start Date</th>
			<th>Duration</th>
			<th>End Date</th>
			<th>Location</th>
		</thead>
		<tbody>
			<tr>
				<?php $result = get_project_details($projectID);?>
				<td><?php echo $projectID; ?></td>
				<td><?php echo $result['name']; ?></td>
				<td><?php echo $result['startDate']; ?></td>
				<td><?php echo $result['duration']; ?></td>
				<td><?php echo $result['endDate']; ?></td>
				<td><?php echo $result['Location']; ?></td>
			</tr>
		</tbody>
	</table>

	<?php
}
?>