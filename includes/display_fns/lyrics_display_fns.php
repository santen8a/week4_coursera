<?php

function display_lyrics_body($song_details){
	?>
	<div class="DisplayContainer order-sm-3" id="main_page_lyrics_display">

		<div id="song_title_ribbon">
			<h3><?php echo $song_details['title'] ?></h3>
		</div>

		<div class="row" id="song_details_ribbon">
			<div class="col-sm-4">
				<a href="view_cat.php?catid=<?php echo $song_details['catID'] ?>">><?php echo get_cat_name($song_details['catID'])?></a>
			</div>
			<div class="col-sm-4">
				Artist(s): <?php display_artist_names($song_details['songID']); ?>
			</div>
			<div class="col-sm-4">
				Views: <?php echo $song_details['views']; ?>
			</div>
		</div>

		<p><?php //echo get_song_name($songID)['artist'] ?></p>

		<div id="song_lyrics_ribbon">
			<p>
				<?php display_lyrics($song_details['songID']) ?>
			</p>
		</div>

		<div id="song_lyrics_bottom_ribbon">
			<p>
				Lyrics compiled by: <?php echo get_username($song_details['userID']); ?>
			</p>
		</div>

	</div>
	<?php
}

//displays the artists of the song 
//parameter: songID

function display_artist_names($songID){
	$artists =  get_artist_name($songID);

	if(sizeof($artists)<=1)
	{
		echo $artists[0];
	}
	else
	{
		for($i=0;$i<sizeof($artists);$i++){
			if($i<(sizeof($artists)-1))
			{
				echo "$artists[$i], ";
			}else{
				echo "$artists[$i].";
			}
		}
	}
}

function  listCategoriesOptions(){
	$conn = db_connect();

	$query = "select * from categories";

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request", 1);
	}

	if($result->num_rows==0){
		echo "Category not available.";
	}

	while ($row=$result->fetch_assoc()) {
		echo "<option value='".$row['catID']."'>";
		echo $row['name'];
		echo "</option>";
	}

	$conn->close();

}

function listArtistOptions(){
	$conn = db_connect();

	$query = "select * from artists";

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request", 1);
	}

	if($result->num_rows==0){
		echo "Category not available.";
	}

	while ($row=$result->fetch_assoc()) {
		echo "<option value='".$row['artistID']."'>";
		echo $row['name'];
		echo "</option>";
	}

	$conn->close();
}

function display_song_list($list){
	echo "<ol>";
	foreach ($list as $key) {

		$artists = get_artist_name($key['songID']);
		if(sizeof($artists)<=1)
			echo "<li><a href='view_song.php?id=".$key['songID']."'>".$key['title']." - <small>".$artists[0]."</small></a></li>";
		else if(sizeof($artists)>1)
		{
			echo "<li><a href='view_song.php?id=".$key['songID']."'>".$key['title']." - <small>";
			for($i=0;$i<sizeof($artists);$i++)
			{
				if($i<(sizeof($artists)-1))
					echo "$artists[$i],";
				else
					echo "$artists[$i]";
			}
			echo "</small></a></li>";

		}

	}
	echo "</ol>";

}

function display_song_list_alphabetically(){

	//array of assoc arrays [0]('songID','title')
	$list = get_song_list();
	$alphabets = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

	$sectionStart = true;

	for($i=0;$i<sizeof($list);$i++){


		$startingLetter = substr($list[$i]['title'],0,1);
		if($sectionStart)
		{
			echo "<p class='lyricsSectionHeader'>".$startingLetter."</p>";

			echo "<a href='view_song.php?id=".$list[$i]['songID']."'>".$list[$i]['title']."</a><br>";
			$sectionStart = false;
		}else{
			echo "<a href='view_song.php?id=".$list[$i]['songID']."'>".$list[$i]['title']."</a><br>";
		}

		if($i<(sizeof($list)-1))
		{
			$nextStartingLetter = substr($list[$i+1]['title'],0,1);
			if($startingLetter != $nextStartingLetter){
				$sectionStart = true;
				echo "<hr>";
			}
		}

		
		

	}

}


function display_song_list_startAlpha($startAlpha){

	//array of assoc arrays [0]('songID','title')
	$list = get_song_list_alpha($startAlpha);

	if($list)
	{
		for($i=0;$i<sizeof($list);$i++){

			echo "<p class='lyricsSectionHeader'>".$startAlpha."</p><hr>";


			echo "<a href='view_song.php?id=".$list[$i]['songID']."'>".$list[$i]['title']."</a><br>";
		}
	}else{
		echo "<p class='lyricsSectionHeader'>".$startAlpha."</p><hr>";

		echo "Sorry. No songs in the list yet. You can contribute to improve this section.";
	}
	

}

function display_most_viewed_box(){
	?>
	<div class="contentBox1 order-sm-6">
		<div class="contentBox1_header text-center">
			<h5>Most viewed of the month</h5>
		</div>
		<div class="contentBox1_body">
			<?php display_song_list(get_most_viewed(5)); ?>
		</div>
	</div>

	<?php
}
function display_soundCloud_box(){
	?>
	<?php $songLink ="https://soundcloud.com/5mb-studio/karp-da-marp_drakcin-gyalmo_ii_the-mad-monk5mb-studio&color=%23ff5500"; ?>
	
	<div id="soundCloudBox order-sm-2">
		<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=<?php echo $songLink; ?>&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&single_active=true"></iframe>
	</div>
	

	<?php

}

function display_artist_list_alphabetically(){

	//array of assoc arrays [0]('artistID','name')
	$list = get_artist_list();
	$alphabets = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

	$sectionStart = true;

	for($i=0;$i<sizeof($list);$i++){


		$startingLetter = substr($list[$i]['name'],0,1);
		if($sectionStart)
		{
			echo "<p class='lyricsSectionHeader'>".$startingLetter."</p>";

			echo "<a href='view_song_list.php?artistID=".$list[$i]['artistID']."'>".$list[$i]['name']."</a><br>";
			$sectionStart = false;
		}else{
			echo "<a href='view_song_list.php?artistID=".$list[$i]['artistID']."'>".$list[$i]['name']."</a><br>";
		}

		if($i<(sizeof($list)-1))
			$nextStartingLetter = substr($list[$i+1]['name'],0,1);

		if($startingLetter != $nextStartingLetter){
			$sectionStart = true;
			echo "<hr>";
		}
		

	}

}

function display_song_list_of_artist($artistID){

	//array of assoc arrays [0]('songID','title')
	$list = get_song_list_of_artist($artistID);

	if($list){
		echo "<table class='table'>
		<thead>
		<tr>
		<th scope=\"col\">Song title</th>
		<th scope=\"col\">Year</th>
		<th scope=\"col\">Category</th>
		<th scope=\"col\">Views</th>
		</tr>
		</thead>
		<tbody>";


		foreach ($list as $row) {
			$songDetails = get_song_details($row);
			echo "<tr>";
			echo "<td><a href=\"view_song.php?id=".$row."\">".$songDetails['title']."</a></td>";
			echo "<td>".$songDetails['publishedYear']."</td>";
			echo "<td>".get_cat_name($songDetails['catID'])."</td>";
			echo "<td>".$songDetails['views']."</td>";
			echo "<tr>";
		}

		echo "</tbody>
		</table>";
	}else{
		display_alert_danger("No songs uploaded yet.");
	}
	
	
}


?>