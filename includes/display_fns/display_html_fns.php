<?php 

function display_header($title){
	?>
	<doctype html>
		<html lang="en">
		<head>
			<!-- Required meta tags -->
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

			<!-- Bootstrap CSS -->
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

			
			<link rel="stylesheet" href="/cssStyle.css">
			
			<!--Bootstrap Select-->
			<link rel="stylesheet" href="/select2-bootstrap4.css">

			<title><?php echo $title;?></title>
		</head>
		<body>

			<?php
			if(isset($_SESSION['valid_user']))
				display_navbar_loggedIn();
			else{
				display_navbar_notLoggedIn();
			}
			?>
			
			<div class="container">			
				<?php
			}

			function display_navbar_notLoggedIn(){
				?>

				<nav class="navbar navbar-expand-lg navbar-light bg-light" id="page_main_navbar">
					<div class="container">
						<a class="navbar-brand" href="/index.php"><img src="/img/logo.png" width="30"></a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarNav">
							<ul class="navbar-nav mr-auto navbar-left">
								<li class="nav-item"><a href="/index.php" class="nav-link">Home</a></li>

								<li class="nav-item"><a href="#" class="nav-link">About us</a></li>
							</ul>

							<ul class="nav navbar-nav navbar-right">
								<li class="nav-item"><a href="login.php" class="nav-link">Login</a></li>
								<li class="nav-item"><a href="register_form.php" class="nav-link">Register</a></li>
							</ul>
						</div>		
					</div>

				</nav>

				<?php
			}

			function display_navbar_loggedIn(){
				?>
				<nav class="navbar navbar-expand-lg navbar-light bg-light" id="page_main_navbar">
					<div class="container">
						<a class="navbar-brand" href="/index.php"><img src="/img/logo.png" width="30"></a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarNav">
							<ul class="navbar-nav mr-auto navbar-left">
								<li class="nav-item"><a href="/index.php" class="nav-link">Home</a></li>


								<li class="nav-item"><a href="#" class="nav-link">About us</a></li>
							</ul>

							<ul class="nav navbar-nav navbar-right">
								<li class="nav-item dropdown mr-auto">
									<a href="#" class="nav-link dropdown-toggle" id="user_account_functions" data-toggle="dropdown">
										<div id="userAcc_navbar_holder">
											<img id="userIdIcon" src="svg/si-glyph-person-2.svg"/>
											ID: <?php echo $_SESSION['valid_user']?>			
										</div>

									</a>
									<div class="dropdown-menu dropdown-menu-right" aria-labelledby = "user_account_functions">
										<a class="dropdown-item" href="../logout.php">Logout</a>
										<a class="dropdown-item" href="#">User Page</a>
										<a class="dropdown-item" href="../change_password_form.php">Change Password</a>
									</div>

								</li>
							</ul>
						</div>
					</nav>
				</div>


				<?php
			}

			function display_footer(){
				?>

			</div>
			<footer class="page-footer font-small blue">

				<!-- Copyright -->
				<div class="footer-copyright text-center py-3">© 2018 Copyright:
					<a href="#">SanMountain.com</a>
				</div>
				<!-- Copyright -->

			</footer>

			<!--BOOTSTRAP 4 - JAVASCRIPT -->
			<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
			<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

			<!--JQUERY-->
			<script
			src="https://code.jquery.com/jquery-3.3.1.js"
			integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
			crossorigin="anonymous"></script>

			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


		</body>
		</html>

		<?php
	}

	function display_login_form(){
		if(isset($_SESSION['valid_user'])){
			display_alert_danger("Already logged in as : <b>".$_SESSION['valid_user']."</b><br> Please log out first to log in as new user.");
		}else{
			?>

			<div class="card card-form">

				<div class="card-header">
					<h1>Login</h1>
				</div>

				<div class="card-body">
					<form name="login_form" method="post" action="member.php">
						<div class="form-group">
							<label for="username">Username</label>
							<input class="form-control" type="text" name="username" id="username" placeholder="Email or Username">
						</div>

						<div class="form-group">
							<label for="pwd">Password</label>
							<input class="form-control" type="password" name="password" id="pwd" placeholder="Password">
						</div>

						<button class="btn btn-primary" type="submit" name="submit">Login</button>

					</form>
				</div>

				<div class="card-footer">
					<p class="font-italic">For accesssing full functionality, please login</p>
				</div>
			</div>

			
			<?php
		}
	}

	function display_alert_danger($msg){
		?>

		<div class="alert alert-danger">
			<?php echo $msg; ?>
		</div>
		
		<?php
	}

	function display_alert_danger_list($msg){
		?>

		<div class="alert alert-danger">
			<p class="sectionHeaderH1">Error:</p>
			<ul>
				<?php 
				$msgList = explode('.', $msg);
				foreach ($msgList as $key) {
					if($key!="")
						echo "<li>".$key."</li>";
				}
				?>
			</ul>
		</div>
		<?php

	}

	function display_alert_success($msg){
		?>
		<div class="alert alert-success">
			<?php echo $msg; ?>
		</div>
		<?php
	}

	function display_heading($header){
		?>
		<h1>
			<?php echo $header; ?>
			<hr>
		</h1>
		<?php
	}
	function display_register_form(){
		?>
		<div class="card card-form">
			<div class="card-header">
				<h1>Sign-up</h1>
			</div>
			<div class="card-body">
				<form name="register_form" method="post" action="register_action.php">
					<div class="form-group">
						<label for="email">Email</label>
						<input class="form-control" type="text" name="email" id="email" placeholder="Valid Email Address">
					</div>

					<div class="form-group">
						<label for="username">Username</label>
						<input class="form-control" type="text" name="username" id="username" placeholder="Preferred Username">
					</div>

					<div class="form-group">
						<label for="pwd">Password</label>
						<input class="form-control" type="password" name="password" id="pwd" placeholder="Password (between 6 and 16 characters)">
					</div>

					<div class="form-group">
						<label for="pwd2">Password</label>
						<input class="form-control" type="password" name="password2" id="pwd2" placeholder="Confirm your password.">
					</div>

					<button class="btn btn-primary" type="submit" name="submit" value="requestRegister">Register</button>

				</form>
			</div>
			
		</div>

		<?php
	}

	function display_change_password_form(){
		?>

		<form name="change_password_form" method="post" action="change_password_action.php">

			<div class="form-group">
				<label for="oldpwd">Old Password</label>
				<input class="form-control" type="password" name="oldpwd" id="oldpwd" placeholder="Enter your old password.">
			</div>

			<div class="form-group">
				<label for="pwd">Password</label>
				<input class="form-control" type="password" name="password" id="pwd" placeholder="Password (between 6 and 16 characters)">
			</div>

			<div class="form-group">
				<label for="pwd2">Password</label>
				<input class="form-control" type="password" name="password2" id="pwd2" placeholder="Confirm your password.">
			</div>

			<button class="btn btn-primary" type="submit" name="submit" value="requestChangePassword">Change Password</button>

		</form>
		<?php
	}

	function display_upload_new_song_form(){
		?>

		

		<?php
	}
	function display_nav_function(){
		?>
		<div class="card" id="main-page-options-holder">
			<!--<img src="../img/only_temple.png" class="card-img-top">-->
			<ul class="list-group" id="main-page-list">
				<li class="list-group-item"><a href="../view_song_list.php">Search for Lyrics</a></li>
				<li class="list-group-item"><a href="../view_artist_list.php">Artists</a></li>
				<li class="list-group-item"><a href="../uploadSong">Upload new lyrics</a></li>
			</ul>			
		</div>

		<?php
	}

	function display_upload_song_form(){
		?>
		<div class="upload_song_container">
			<div class="upload_song_container_header">
				<h1 class="sectionHeaderH1">Upload new song</h1>
			</div>
			<div class="upload_song_container_body">
				<form name="upload_song_form" method="post" action="action">
					<div class="form-group">
						<label for="song_title">Song Title</label>
						<input class="form-control" type="text" name="song_title" id="song_title" placeholder="Song title">
					</div>
					<hr>
					<div class="form-group" id="artistSelectForm">
						<label for="artist">Artist</label><br>
						<select class="select2 artistSelect" name="artists[]" multiple="multiple" style="width: 100%">
							<?php listArtistOptions();?>
						</select>
						<p id="artistRegisterLink"><a href="view_artist_list.php#artistNameFormDiv">*Artist not in the list yet? Click here to register the artist.</a> </p>
					</div>


					<hr>
					<div class="form-group" id="catSelectFormGroup">
						<label for="catSelect">Category</label>
						<select class="form-control" name="category" id="catSelect">
							<?php listCategoriesOptions();?>
						</select>
					</div>
					<hr>
					<div class="form-group" id="year">
						<label for="year">Year Published</label>
						<input class="form-control" type="text" name="song_year" id="song_year" placeholder="eg. 2015">
					</div>
					<hr>
					<div class="form-group">
						<label for="lanSelect">Language</label>
						<select class="form-control" name="language" id="lanSelect">
							<option value="1">English</option>
							<option value="2">རྫོང་ཁ</option>
						</select>
					</div>
					<hr>
					<div class="form-group">
						<label for="lyrics">Lyrics</label>
						<textarea rows=10 class="form-control" name="lyrics" id="lyrics" placeholder="Lyrics goes here..."></textarea>
					</div>

					<button class="btn btn-primary" type="Submit" name="submit" value="requestNewSongUpload">Upload</button>

				</form>
			</div>
			
		</div>

		<?php

	}

	function display_footer_select2(){
		?>

	</div>
	<footer class="page-footer font-small blue">

		<!-- Copyright -->
		<div class="footer-copyright text-center py-3">© 2018 Copyright:
			<a href="#">SunMountain.com</a>
		</div>
		<!-- Copyright -->

	</footer>



	<!--JQUERY-->
	<script
	src="https://code.jquery.com/jquery-3.3.1.js"
	integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
	crossorigin="anonymous"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


	<!--BOOTSTRAP 4 - JAVASCRIPT -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>



	<!--select2-->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

	<script>
		$(document).ready(function() {
			$('.artistSelect').select2({
				placeholder: 'Select an option'
			});

			$('.artistSelect').select2({
				theme: 'bootstrap4'
			});
		});
	</script>

</body>
</html>
<?php
}
function display_register_link(){
	?>
	<div class="alert alert-info order-1">
		<?php echo "Not registered yet? <a href='register_form.php'>Click here to Register</a>"; ?>
	</div>

	<?php
}

function display_alpha_song_links(){
	?>
	<div class="navbar" id="alphaSearch">
		<ul class="linklist">
			<li><a href='songlist.php?start=#' title='#'>#</a></li>
			<li><a href='songlist.php?start=A' title='A'>A</a></li>
			<li><a href='songlist.php?start=B' title='B'>B</a></li>
			<li><a href='songlist.php?start=C' title='C'>C</a></li>
			<li><a href='songlist.php?start=D' title='D'>D</a></li>
			<li><a href='songlist.php?start=E' title='E'>E</a></li>
			<li><a href='songlist.php?start=F' title='F'>F</a></li>
			<li><a href='songlist.php?start=G' title='G'>G</a></li>
			<li><a href='songlist.php?start=H' title='H'>H</a></li>
			<li><a href='songlist.php?start=I' title='I'>I</a></li>
			<li><a href='songlist.php?start=J' title='J'>J</a></li>
			<li><a href='songlist.php?start=K' title='K'>K</a></li>
			<li><a href='songlist.php?start=L' title='L'>L</a></li>
			<li><a href='songlist.php?start=M' title='M'>M</a></li>
			<li><a href='songlist.php?start=N' title='N'>N</a></li>
			<li><a href='songlist.php?start=O' title='O'>O</a></li>
			<li><a href='songlist.php?start=P' title='P'>P</a></li>
			<li><a href='songlist.php?start=Q' title='Q'>Q</a></li>
			<li><a href='songlist.php?start=R' title='R'>R</a></li>
			<li><a href='songlist.php?start=S' title='S'>S</a></li>
			<li><a href='songlist.php?start=T' title='T'>T</a></li>
			<li><a href='songlist.php?start=U' title='U'>U</a></li>
			<li><a href='songlist.php?start=V' title='V'>V</a></li>
			<li><a href='songlist.php?start=W' title='W'>W</a></li>
			<li><a href='songlist.php?start=X' title='X'>X</a></li>
			<li><a href='songlist.php?start=Y' title='Y'>Y</a></li>
			<li><a href='songlist.php?start=Z' title='Z'>Z</a></li>

		</ul>
	</div>

	<?php
}
?>