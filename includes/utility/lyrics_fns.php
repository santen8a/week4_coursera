<?php

function display_lyrics($songID){

	$conn = db_connect();

	$query = "select * from lyrics where songID = '".$songID."'";

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request", 1);
	}

	if($result->num_rows==0){
		echo "Song not available.";
	}

	echo nl2br($result->fetch_assoc()['lyrics']);

	$conn->close();

}

//get_song_name
//Parameter(songID)
//return: string, the song title.
function get_song_name($songID){
	$conn = db_connect();

	$query = "select title from songs where songID = '".$songID."'";

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request");
	}

	if($result->num_rows==0){
		echo "Song not available.";
	}


	$conn->close();
	return $result->fetch_assoc()['title'];
}


//get_song_details
//Parameter(songID)
//return: assoc array of that song.
function get_song_details($songID){
	$conn = db_connect();

	$query = "select * from songs where songID = '".$songID."'";

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request");
	}

	if($result->num_rows==0){
		echo "Song not available.";
	}

	$conn->close();
	return $result->fetch_assoc();
}

function add_category($name,$description){
	$conn = db_connect();

	$query = "insert into categories (name,description) values ('$name','$description')";

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request");
	}

	$conn->close();
	return true;
}

//get_cat_name
//Parameter(catID)
//return: category name that song.
function get_cat_name($catID){
	$conn = db_connect();

	$query = "select name from categories where catID = '".$catID."'";

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request");
	}

	if($result->num_rows==0){
		echo "Category not found.";
	}

	$conn->close();
	return $result->fetch_assoc()['name'];
}

function increment_views($songID,$song_details){
	$conn = db_connect();

	$query = "UPDATE songs SET views = ((".$song_details['views'].")+1) WHERE songID = '".$songID."'";

		//(SELECT views from songs WHERE songID = '$songID')
	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request");
	}

	$conn->close();
	return true;
}

//get_artist_name
//Parameter(songID)
//return: array of artist names of that song.
function get_artist_name($songID){
	$conn = db_connect();

	$query = "select a.name	from artists a INNER JOIN artist_song b ON a.artistID = b.artistID WHERE songID = '".$songID."'";
	//$query = "select a.name from artists a INNER JOIN artist_song b ON a.artistID = b.artistID WHERE songID = '22'";

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request");
	}
	if($result->num_rows==0){
		return $artists[] ="Arist not found.";
	}else{
		while ($row = $result->fetch_assoc()) {
			$artists[] = ($row['name']);
		}		
	}


	$conn->close();
	return $artists;
}

//get_artist_name
//Parameter(songID)
//return: array of artist names of that song.
function get_artist_name_from_id($artistID){
	$conn = db_connect();

	$query = "SELECT name FROM artists where artistID = '".$artistID."'";

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request");
	}
	if($result->num_rows==0){
		return $artists[] ="Arist not found.";
	}else{
		while ($row = $result->fetch_assoc()) {
			$artists[] = ($row['name']);
		}		
	}


	$conn->close();
	return $artists;
}

//get_song_list
//gives an array of assocaiative list of the songs in DB
function get_song_list(){

	$conn = db_connect();

	$query = "SELECT songID,title FROM songs ORDER BY title ASC";

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request");
	}

	while ($row = $result->fetch_assoc()) {
		$list[] = $row;
	}

	$conn->close();
	return $list;
}

//get_song_list_alpha
//gives an array of assocaiative list of the songs starting with that alphabet in DB
function get_song_list_alpha($startAlpha){

	$conn = db_connect();

	if($startAlpha='#'){
		$query = "SELECT songID,title FROM songs WHERE title REGEXP '^(1|2|3|4|5|6|7|8|9|0|`)' ORDER BY title ASC";
	}else{
		$query = "SELECT songID,title FROM songs WHERE title LIKE '".$startAlpha."%' ORDER BY title ASC";
	}
	

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request");
	}

	if($result->num_rows==0)
		return false;

	while ($row = $result->fetch_assoc()) {
		$list[] = $row;
	}

	$conn->close();
	return $list;
}

function get_most_viewed($limit){
	$conn = db_connect();

	$query = "SELECT songID,title FROM songs ORDER BY views DESC LIMIT ".$limit;

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request");
	}

	while ($row = $result->fetch_assoc()) {
		$list[] = $row;
	}

	$conn->close();
	return $list;
}


//get_artist_list
//gives an array of assocaiative list of the songs in DB
function get_artist_list(){

	$conn = db_connect();

	$query = "SELECT artistID,name FROM artists ORDER BY name ASC";

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request");
	}

	while ($row = $result->fetch_assoc()) {
		$list[] = $row;
	}

	$conn->close();
	return $list;
}

//get_song_list
//gives an array of assocaiative list of the songs in DB
function get_song_list_of_artist($artistID){

	$conn = db_connect();

	$query = "SELECT songID FROM artist_song WHERE artistID = '".$artistID."'";

	$result = $conn->query($query);

	if(!$result){
		throw new Exception("Error Processing Request");
	}

	if($result->num_rows==0)
		return false;

	while ($row = $result->fetch_assoc()) {
		$list[] = $row['songID'];
	}

	$conn->close();
	return $list;
}

?>