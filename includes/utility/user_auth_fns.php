<?php

function login($username,$password){
	$conn = db_connect();

	$result = $conn->query("select * from users where username = '".$username."' AND passwd = sha('".$password."')");

	if(!$result){
		throw new Exception('Could not log you in.');	
	}

	if($result->num_rows>0){
		return true;
	}else{
		throw new Exception('The username and password do not match.');
	}

	$result->free();
	$conn->close();
}

function check_valid_user(){

	if(isset($_SESSION['valid_user']))
	{
		//login verified
		display_alert_success("Logged in as: ".$_SESSION['valid_user']);
	}else{
		header('location: ../login.php?errMsg=You are not logged in. Please log in to continue.');
		exit;
	}

}

function is_valid_user(){

	if(isset($_SESSION['valid_user']))
	{
		//login verified
		return true;
	}else{
		return false;
	}

}

function filled_out($array){
	foreach ($array as $key => $value) {	
		if((!isset($key)) || ($value=='')){
			return false;
		}
	}
	return true;
}

function valid_email($email){
	if (preg_match('^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$^', $email)) {
		return true;
	} else {
		return false;
	}
}

function register($email,$username,$password){
	//make connection
	$conn = db_connect();

	//check if the username is available.
	$usernameCheckDb = $conn->query("select * from users where username = '".$username."'");

	if(!$usernameCheckDb){
		throw new Exception("Error Processing Request");
		
	}
	if($usernameCheckDb->num_rows>0){
		throw new Exception('Username taken. Please choose another username and try again');
	}

	//check if the email is already used.
	$emailCheckDb = $conn->query("select * from users where email = '".$email."'");
	if(!$emailCheckDb){
		throw new Exception("Error Processing Request");
		
	}
	if($emailCheckDb->num_rows>0){
		throw new Exception('Email is currently linked with a existing account. Please try resetting your password.');
	}

	//if above list are checked , register user into database
	$registerUser = $conn->query("insert into users(username,passwd,email) values ('".$username."',sha1('".$password."'),'".$email."')");

	if(!$registerUser){
		throw new Exception("Error Processing Request. Could not register user into system. Please try again later");
	}	

	return true;

	$usernameCheckDb->free();
	$conn->close();

}

function getUserID($username){
	//make connection
	$conn = db_connect();

	$query = "select * from users where username = '".$username."'";

	$result = $conn->query($query);

	if(!$result){
		throw new Exception('Error retreiving data from server.');	
	}

	if($result->num_rows==0){
		return false;
	}else{
		$result = $result->fetch_assoc();
		return $result['userID'];
	}

}



?>