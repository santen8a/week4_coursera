<?php

function clean($data){

//escape html inputs
	$data = htmlspecialchars($data);

//Strip unnecessary characters
	$data = trim($data);

//remove backslashes
	$data = stripcslashes($data);		

return $data;
}



?>