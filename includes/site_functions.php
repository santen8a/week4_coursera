<?php
	require_once('display_fns/display_html_fns.php');
	require_once('utility/db_fns.php');
	require_once('utility/user_auth_fns.php');
	require_once('utility/misc.php');
	require_once('utility/lyrics_fns.php');
	require_once('utility/admin_fns.php');
	require_once('display_fns/lyrics_display_fns.php');
	require_once('utility/user_fns.php');
	require_once('utility/validation_fns.php');
	require_once('utility/work_fns.php');
	require_once('display_fns/work_display_fns.php');
	define('SITENAME', 'SunMountain Media')
?>