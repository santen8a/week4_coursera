<?php

require_once('../includes/site_functions.php');
session_start();

$old_user = @$_SESSION['valid_user'];

unset($_SESSION['valid_user']);
$sess_destroy = session_destroy();

display_header("Logging Out");

echo "<div class='row justify-content-center'>
		<div class='col-md-6 col-sm-8'>";
if(!empty($old_user)){

	if($sess_destroy){
		display_alert_success("Logged out successfully. Please visit us again.");
		
		display_login_form();	
		
	}else{
		display_alert_danger("Log out failed.");
	}

}else{
	display_alert_danger("Log out failed. User has not logged in. Please log in first.");
	display_login_form();	
}

echo "</div>
		</div>";

display_footer();

?>