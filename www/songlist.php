<?php
require_once('../includes/site_functions.php');
session_start();



//Redirect user to index.php is the start variable has not been set.
if(!isset($_GET['start']))
{
	header('Location: index.php');
}

$alphaStart = clean($_GET['start']);
display_header("Song list: ".$alphaStart);
?>
<div class="row">
	<div class="col-md-4">
		
	</div>

	<div class="col-md-8">
		<div class="DisplayContainer">
			<div class="containerHeader">
				<h3>Songs</h3>
			</div>
			<div class="containerBody">
				<?php display_song_list_startAlpha($alphaStart);?>
			</div>
		</div>
	</div>
</div>


<?php
display_footer();
?>