<?php
define("BASE_URL", "/www/");
require_once('../includes/site_functions.php');
session_start();
display_header('Home');
?>
<div class="row">
	<div class="col-sm-12">
		<?php display_alpha_song_links(); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-4 push-sm-8">
		<?php display_nav_function(); ?>
	</div>

	<div class="col-md-8 pull-sm-4">
		<div class="jumbotron" id="welcomeMessageJumbo">
			<h4>Welcome to Lyrics-Bhutan.com</h4>
			<hr class="jumbotron-hr">
			<p>Your gateway to the largest Bhutanese song lyrics collection. Please help us build the largest bhutanese song lyrics database.</p>
		</div>
	</div>
</div>

<div class="row" id="content_main">
	<div class="col-md-4 push-sm-8">
		<?php display_most_viewed_box(); ?>
	</div>
	<div class="col-md-8 pull-sm-4">
		<?php 
		$songID = 43;
		$song_details = get_song_details($songID);
		display_lyrics_body($song_details); 
		?>
	</div>


</div>
<!--MAIN ROW ENDS HERE-->



<?php
display_footer();
?>