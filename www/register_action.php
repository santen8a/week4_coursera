<?php

require_once('site_functions.php');
session_start();

$email = @$_POST['email'];
$username = @$_POST['username'];
$password = @$_POST['password'];
$password2 = @$_POST['password2'];


//if user is logged in , deny access.
if(isset($_SESSION['valid_user']))
{
	header('location: index.php');
}


try{
	//check if all the forms are filled
	if(!filled_out($_POST)){
		throw new Exception('Please fill in the registration form completely.');
	}

	//check if email is valid
	if(!valid_email($email))
	{
		throw new Exception('Invalid email address. Please try again with a valid email address.');
	}

	//if both the password are same
	if(!($password==$password2)){
		throw new Exception('The passwords you entered do not match. Please try again');
	}

	//check if password length is ok
	if(strlen($password)<6||strlen($password)>16){
		throw new Exception('Password should be between 6 and 16 characters');
	}

	//if all clear, then register user
	register($email,$username,$password);

	$_SESSION['valid_user'] = $username;

	header('location: index.php');
	//header('location : member.php');
	//if register successful, 1. set session variable 2. display member page
}catch(Exception $e){
	display_header("Error:");
	display_alert_danger("Registration failed : ".$e->getMessage());
	display_register_form();
	display_footer();
}

?>