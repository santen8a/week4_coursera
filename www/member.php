<?php

require_once('../includes/site_functions.php');
session_start();


$username = @$_POST['username'];
$password = @$_POST['password'];

if(isset($username) && isset($password)){
	try{
		login($username,$password);
		$_SESSION['valid_user'] = $username;
	}
	catch(Exception $e){
		display_header("Error");
		echo "<div class='row'>
		<div class='col-md-6 col-sm-8'>";
		display_alert_danger("Error: ".$e->getMessage()." Please try again.");
		display_login_form();	
		echo "</div>
		</div>";
		display_footer();
		exit;
	}
}
display_header('Home');
check_valid_user();
display_footer();

?>