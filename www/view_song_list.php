<?php

require_once('../includes/site_functions.php');
session_start();
$artistID = @clean($_GET['artistID']);
display_header("Song Book");

?>

<div class="row" id="content_main">
	<div class="col-md-4">
		<?php display_nav_function(); ?>

		<?php display_most_viewed_box(); ?>

	</div>

	<div class="col-md-8">
		<div class="DisplayContainer">
			<div class="containerHeader">
				<?php
				if(!isset($_GET['artistID']))
					echo "<h4>Songs</h4>";
				else if(isset($_GET['artistID'])){
					echo "<h4>Artist: ".get_artist_name_from_id($artistID)[0]."</h4>";
				}?>
			</div>
			<div class="containerBody">

				<?php
				if(!isset($_GET['artistID']))
					display_song_list_alphabetically();
				else if(isset($_GET['artistID'])){
					display_song_list_of_artist($artistID);
				}


				?>
			</div>
			
		</div>
	</div>
</div>
<!--MAIN ROW ENDS HERE-->

<?php
display_footer();
?>