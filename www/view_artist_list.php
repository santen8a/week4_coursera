<?php

require_once('../includes/site_functions.php');
session_start();

display_header("Song Book");
class formErrorException extends Exception{}
$errMsg = "";
$topMSG = "";

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['artistSubmit'])){
	try{

		if(empty($_POST['artistName']))
			throw new formErrorException("Artist name was not entered.");


		if(strlen($_POST['artistName']>100))
			throw new formErrorException("Artist name exceeds 100 letters.");

			//make connection
		$conn = db_connect();

		$artistName = clean($_POST['artistName']);

		//check if the username is available.
		$artistNameDB = $conn->query("select * from artists where name = '".$artistName."'");

		if(!$artistNameDB){
			throw new Exception("Error Processing Request");

		}
		if($artistNameDB->num_rows>0){
			throw new formErrorException('Artist already in the list.');
		}
		


		//all checks done, proceed to enter into DB
		$query = "INSERT INTO artists (name) VALUES ('".$artistName."')";

		$result = $conn->query($query);

		if(!$result){
			throw new Exception("Error Processing Request", 1);
		}else{
			$topMSG = "New artist added to the list.";
		}

	}
	catch(formErrorException $e){
		$errMsg = $e->getMessage();
	}
	catch(Exception $e){
		echo "ompalompa";
		display_alert_danger($e->getMessage());
		display_footer();
		exit;
	}
	
}

?>

<div class="row" id="content_main">
	<div class="col-md-4">
		<?php display_nav_function(); ?>

		<?php display_most_viewed_box(); ?>

	</div>

	<div class="col-md-8">
		<?php if(!empty($topMSG)){ display_alert_success($topMSG); unset($topMSG);} ?>
		<div class="DisplayContainer col-md-12">
			<div class="containerHeader">
				<h3>Artists</h3><hr>

			</div>
			<div class="containerBody">
				<?php display_artist_list_alphabetically();?>
			</div>
			
		</div>

		<div>
			<div class="contentBox1 col-md-12" id="artistNameFormDiv">
				<div class="contentBox1_header">
					<h4>Artist not in the list yet? Add artist here!</h4>
				</div>
				<div class="contentBox1_body">
					<?php if(!empty($errMsg)){ display_alert_danger($errMsg); unset($errMsg);} ?>
					<form method="post" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
						<div class="form-group">
							<label for="artistName">Arist:</label>
							<input type="text" name="artistName" id="artistName" class="form-control">
						</div>
						<button type="submit" class="btn btn-primary" name="artistSubmit">Add</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--MAIN ROW ENDS HERE-->

<?php
display_footer();
?>