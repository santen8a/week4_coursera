<?php

require_once('../../includes/site_functions.php');
session_start();

$_SESSION['uploading'] = 1;

display_header('Upload new song.');

check_valid_user();

?>
<div class="row justify-content-center">
	<div class="col-sm-8">
		<?php display_upload_song_form(); ?>
	</div>
</div>

<?php display_footer_select2(); ?>
