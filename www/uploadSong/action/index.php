<?php

require_once('site_functions.php');
session_start();

//LOGGED IN CHECK
//if user is not logged in , deny access.
if(!isset($_SESSION['valid_user']))
{
	header('location: login.php');
}

//STORE USER INPUT
$userID = 'GUEST';
$userID = @ getUserID($_SESSION['valid_user']);

$song_title = @$_POST['song_title'];

$artists = @$_POST['artists'];

$category = @$_POST['category'];

$song_year = @$_POST['song_year'];

$language = @$_POST['language'];

$lyrics = @$_POST['lyrics'];


//VALIDATION


//2. SELECTED VALUES FOR ARTIST,CATEGORY,LANGUAGE

//3.VALIDATION FOR LYRICS
try{
	//MASTER KEY: APPROVED.
	$approved = true;
	//make sure all the feilds are set
	if(!(isset($song_title)||isset($artists)||isset($category)||isset($category)||isset($language)||$isset($lyrics)||isset($song_year))){
		$errMsg[]="Please fill in all the required informations.";
		$approved = false;
	}

	//Check if the feilds are empty and record their empty status in assoc array
		if(empty($song_title))
		{
			$errMsg[] = "Song title is empty.";
			$approved = false;
		}
		if(empty($artists))
		{
			$errMsg[] = "Please enter song artist(s).";
			$approved = false;
		}
		if(empty($category))
		{
			$errMsg[] = "Please select song category.";
			$approved = false;
		}
		if(empty($language))
		{
			$errMsg[] = "Please select song language.";
			$approved = false;
		}
		if(empty($lyrics))
		{
			$errMsg[] = "Please enter song lyrics.";
			$approved = false;
		}
		if(empty($song_year))
		{
			$errMsg[] = "Please enter the year the song was published.";
			$approved = false;
		}



	//song title should be (<100 CHAR)	
		if(strlen($song_title)>100)
		{
			$errMsg[] = "Letters in \"Song title\" should be less than 100.";
			$approved = false;
		}
		


	//limit characters in lyrics to 50000 character
		if(strlen($lyrics)>50000)
		{
			$errMsg[] = "Charater count in lyrics exceeds limit.";
			$approved = false;
		}


		if(!$approved){
			$errMessage = '';
			foreach ($errMsg as $key) {
				$errMessage.=$key;
			}
			throw new Exception($errMessage);
		}else{
		//ALL VERIFIED , THEN WRITE INTO DATABASE
			$conn = db_connect();

		//songTable entry
			$query = "INSERT INTO songs (userID,catID,title,publishedYear,submit_date) VALUES ('".$userID."','".$category."','".$song_title."','".$song_year."','".($timestamp = date('Y-m-d H:i:s'))."')";

			$result = $conn->query($query);

			if(!$result){
				throw new Exception("Error Processing Request.");
			}

		//obtain songID inserted
			$query = "SELECT songID FROM songs WHERE title='".$song_title."' AND submit_date = '".$timestamp."'";

			$result = $conn->query($query);
			if(!$result){
				throw new Exception("Error Processing Request.");
			}
			$songID =  $result->fetch_assoc()['songID'];

		//artistID entry
			for($i=0;$i<sizeof($artists);$i++)
			{
				$query = "INSERT INTO artist_song (artistID,songID) VALUES ('".$artists[$i]."','".$songID."')";
				$result = $conn->query($query);
				if(!$result){
					throw new Exception("Error Processing Request.");
				}	
			}

			//lyrics entry
			$query = "INSERT INTO lyrics (songID,lyrics) VALUES ('".$songID."','".$lyrics."')";

			$result=$conn->query($query);

			if(!$result){
				throw new Exception("Error Processing Request.");
			}
			$conn->close();
			display_header("Upload done.");
			display_alert_success("Upload Success.");
		}


	}catch(Exception $e){
		display_header('Song upload retry');
		display_alert_danger_list($e->getMessage());
		display_upload_song_form();
		display_footer_select2();
		exit;
	}
	?>


	<!--footer-->
<?php display_footer_select2(); ?>