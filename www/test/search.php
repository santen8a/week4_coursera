<?php
require_once('site_functions.php');

$conn = db_connect();

if(isset($_POST["query"]))
{
	$output = '';
	$query = "SELECT * FROM artists WHERE name LIKE '%".$_POST["query"]."%'";

	$result = mysqli_query($conn, $query);
	$output = "<ul class='list-unstyled'>";

	if(mysqli_num_rows($result) > 0)
	{
		while ($row = mysqli_fetch_array($result)) {
			$output .= '<li>'.$row["name"].'</li>';
		}
	} else{
		$output .= "<li disabled>Artist not found.<button class='btn btn-primary'>Register Artist</button></li>";
	}

	$output .= '</ul>';

	echo $output;
}

?>