<doctype html>
	<html lang="en">
	<head>
		<!-- Required meta tags -->
		<link rel="stylesheet" type="text/css" href="cssStyle.css">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


		<title>Test</title>

		<style type="text/css">
		body{
			margin: 30px;
		}
		.my-container{
			border: 1px solid green;
		}

		.my-row{
			border: 3px solid red;
			height: 200px;
		}

		.my-col{
			border: 3px dotted blue;
		}

	</style>
</head>
<body>

	<div class="container my-container">
		<div class="row my-row">
			<div class="col-md-8 col-sm-6 my-col">
				row 1 col 1 
			</div>	
			<div class="col-md-4 col-sm-6 my-col">
				row 1 col 2 
			</div>	
		</div>

		<div class="row justify-content-between my-row">
			<div class="col-md-4 my-col order-md-12">
				row 2 col 1 
			</div>	
			<div class="col-md-4 my-col align-self-end order-md-2">
				row 2 col 2 
			</div>	
		</div>

	</div>

</body>
</html>