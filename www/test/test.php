
<br>
<doctype html>
	<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="cssStyle.css">

		<title>Logging Out</title>
	</head>
	<body>
		<div class="container-fluid">				
			<nav class="navbar navbar-expand-lg" id="page_main_navbar">
				<a class="navbar-brand" href="#"><img src="../img/logo.png" width="50"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav">
						<li class="nav-item"><a href="#" class="nav-link">Home</a></li>

						<li class="nav-item dropdown">
							<a href="#" class="nav-link dropdown-toggle" id="create_content" data-toggle="dropdown">	Create Content
							</a>
							<div class="dropdown-menu" aria-labelledby = "create_content">
								<a class="dropdown-item" href="#">Submit Lyrics</a>
								<a class="dropdown-item" href="#">Create Band Page</a>
								<a class="dropdown-item" href="#">Create Artist Profile</a>
							</div>

						</li>


						<li class="nav-item"><a href="#" class="nav-link">About us</a></li>
					</ul>					
				</div>
			</nav>

			<br>
			<div class="row">
				<div class="col-md-3">

				</div>
				<div class="col-md-6">
					<div class="alert alert-danger">
					Log out failed. User has not logged in. Please log in first.				</div>
				</div>
				<div class="col-md-3">

				</div>
			</div>


			<form name="login_form" method="post" action="member.php">
				<div class="form-group">
					<label for="username">Username</label>
					<input class="form-control" type="text" name="username" id="username" placeholder="Email or Username">
				</div>

				<div class="form-group">
					<label for="pwd">Password</label>
					<input class="form-control" type="password" name="password" id="pwd" placeholder="Password">
				</div>

				<button class="btn btn-primary" type="submit" name="submit">Login</button>

			</form>
		</div>
		<footer class="page-footer font-small blue">

			<!-- Copyright -->
			<div class="footer-copyright text-center py-3">© 2018 Copyright:
				<a href="#">SanMountain.com</a>
			</div>
			<!-- Copyright -->

		</footer>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	</body>
	</html>


