<?php

require_once('site_functions.php');

display_header("RibbonTest");

?>
<style type="text/css">
h1 {
	text-align: center;
	position: relative;
	color: #fff;    
	margin: 0 -30px 30px -30px;
	padding: 10px 0; 
	text-shadow: 0 1px rgba(0,0,0,.8);
	background-color: #5c5c5c;
	background-image:  linear-gradient(rgba(255,255,255,.3), rgba(255,255,255,0));
	-moz-box-shadow: 0 2px 0 rgba(0,0,0,.3);
	-webkit-box-shadow: 0 2px 0 rgba(0,0,0,.3);
	box-shadow: 0 2px 0 rgba(0,0,0,.3);
}

h1:before, 
h1:after {
	content: '';
	position: absolute;
	border-style: solid;
	border-color: transparent;
	bottom: -10px;
}

h1:before {
	border-width: 0 10px 10px 0;
	border-right-color: #222;
	left: 0;
}

h1:after {
	border-width: 0 0 10px 10px;
	border-left-color: #222;
	right: 0;
}

#mainCon{
	background-color: grey;
}
</style>
<div class="row justify-content-center">
	<div class="col-md-6" id="mainCon">
		<h1>
			hello
		</h1>

		<p>This is a test message</p>
		<p>This is a test message</p>
		<p>This is a test message</p>
		<p>This is a test message</p>
		<p>This is a test message</p>
		<p>This is a test message</p>
		<p>This is a test message</p>
	</div>
	
</div>

<?php
display_footer();

?>