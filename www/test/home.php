<?php
require_once('../include/all_comp.php');
output_header('Home');
?>


<link rel="stylesheet" href="styles.css">

<div class="container-fluid">
	<div class="row">
		<div class="col-md-6" style="background-color: red">test1</div>

		<div class="col-md-6" style="background-color: blue">test2</div>
	</div>


	<div class="row">
		<div class="col-md-4" style="background-color: grey">test1</div>

		<div class="col-md-4" style="background-color: pink">test2</div>

		<div class="col-md-4" style="background-color: orange">test3</div>
	</div>

	<div class="row">
		<div class="col-md-6" style="background-color: violet"><?php for($i=0;$i<=20;$i++){echo 'test1<br>';} ?></div>

		<div class="col-md-6" style="background-color: blue">test2</div>

		<div class="col-md-6 col-md-offset-3" style="background-color: yellow">test3</div>
	</div>

	<div class="row">
		<div class="col-md-8  col-md-push-2" style="background-color: black">test4</div>
		<div class="col-md-7" style="background-color: pink">test5</div>
	</div>

	<div class="row">
		<div class="tab-content">
			<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">...</div>
			<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
			<div class="tab-pane fade" id="messages" role="tabpanel" aria-labelledby="messages-tab">...</div>
			<div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="settings-tab">...</div>
		</div>
	</div>


</div>



<?php
output_footer();
?>