<?php

require_once('site_functions.php');
display_header('Search');
?>

<style type="text/css">
ul{
	background-color: #eee;
	cursor: pointer;
}

li{
	padding: 12px;
}
</style>

<div class="container" style="background-color: white;">
	<div class="row justify-content-center">
		<div class="col-md-6">
			<h3>Auto complete functionality</h3>
			<label>Enter Country Name</label>
			<input type="text" name="country" id="country" class="form-control" placeholder="Search for country">
			<div id="countryList"></div>
		</div>
	</div>
</div>
<br><br><br><br><br>



<script>
	$(document).ready(function(){
		$('#country').keyup(function(){

			var query = $(this).val();
			alert(query);

			if(query != '')
			{
				$.ajax({
					url:"search.php",
					method:"POST",
					data:{query:query},
					success:function(data)
					{
						$('#countryList').fadeIn();
						$('#countryList').html(data);
					}
				});
			}
			else{
				$('#countryList').fadeOut();
				$('#countryList').html("");
			}
		});

		$(document).on('click','li',function(){
			$('#country').val($(this).text());
			$('#countryList').fadeOut();
		});
	});
</script>


<?php
display_footer();
?>


<!--
<script type="text/javascript">
	$(document).ready(function() {
		$('#country').keyup(function() {
			alert('Handler for .keyup() called.');
		});
	});
</script>

-->