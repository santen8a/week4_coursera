<?php

require_once('site_functions.php');
session_start();

display_header('Create new account');

if(isset($_SESSION['valid_user'])){
	display_alert_danger('Access to registration page is denied when you are logged in. First log out to register a new user.');
}else{
?>

<div class="row justify-content-center">
	<div class="col-md-6 col-sm-8">
		<?php  display_register_form();?>
	</div>
</div>


<?php	
	
}

display_footer();

?>