<?php

require_once('site_functions.php');
session_start();

display_header("Changing Password");

$oldpwd = $_POST['oldpwd'];
$password = $_POST['password'];
$password2 = $_POST['password2'];

try{
	//1. check to see if old user password is correct
	
	$conn=db_connect();
	$query = "SELECT passwd FROM users WHERE username = '".$_SESSION['valid_user']."'";

	$oldpwdRequest = $conn->query($query);
	$passwordArray = $oldpwdRequest->fetch_assoc();
	$oldPwdDB = $passwordArray['passwd'];

	if(!$oldPwdDB){
		throw new Exception("Error Processing Request, Please try again later");
	}

	if(!(sha1($oldpwd)==$oldPwdDB)){
		throw new Exception('Incorrect old password. Please enter the correct old password and try again.');
	}


	//check to see if length is ok
	if(strlen($password)<6||strlen($password2)>16){
		throw new Exception('Password should be 6 - 16 characters long.');
	}

	//check to see if two password are same
	if($password!=$password2){
		throw new Exception('The two new password do not match. Please try again');
	}



	//replace new password
	$changePassword = $conn->query("UPDATE users SET passwd = sha1('".$password."') WHERE username = '".$_SESSION['valid_user']."'");

	if(!$changePassword){
		throw new Exception("Error Processing Request, Please try again later");
	}

	$conn->close();


}catch(Exception $e){
	display_heading("Error");
	display_alert_danger('Password change failed : '.$e->getMessage());
	display_change_password_form();
	display_footer();
	exit;
}

display_alert_success('Password change successful.');
display_footer();

?>