<?php

require_once('site_functions.php');
?>

<doctype html>
	<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


		<link rel="stylesheet" href="cssStyle.css">

		<!--Bootstrap Select-->
		<link rel="stylesheet" href="dist/select2-bootstrap4.css">

		<title>sante</title>
	</head>
	<body>

		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="index.php"><img src="logo.png" width="30"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav mr-auto navbar-left">
					<li class="nav-item"><a href="index.php" class="nav-link">Home</a></li>

					<li class="dropdown">
						<a href="#" class="nav-link dropdown-toggle" id="create_content" data-toggle="dropdown">	Create Content
						</a>
						<div class="dropdown-menu" aria-labelledby = "create_content">
							<a class="dropdown-item" href="#">Submit Lyrics</a>
							<a class="dropdown-item" href="#">Create Band Page</a>
							<a class="dropdown-item" href="#">Create Artist Profile</a>
						</div>
					</li>
					<li class="nav-item"><a href="#" class="nav-link">About us</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
							<li class="nav-item"><a href="login.php" class="nav-link">Login</a></li>
							<li class="nav-item"><a href="register_form.php" class="nav-link">Register</a></li>
						</ul>
			</div>
		</nav>


		<!--<nav class="navbar navbar-expand-lg" id="page_main_navbar">
					<div class="navbar-header">
						<a class="navbar-brand" href="index.php"><img src="logo.png" width="30"></a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>						
					</div>
					
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="nav navbar-nav mr-auto navbar-left">
							<li class="nav-item"><a href="index.php" class="nav-link">Home</a></li>

							<li class=" dropdown">
								<a href="#" class="nav-link dropdown-toggle" id="create_content" data-toggle="dropdown">	Create Content
								</a>
								<div class="dropdown-menu" aria-labelledby = "create_content">
									<a class="dropdown-item" href="#">Submit Lyrics</a>
									<a class="dropdown-item" href="#">Create Band Page</a>
									<a class="dropdown-item" href="#">Create Artist Profile</a>
								</div>
							</li>
							<li class="nav-item"><a href="#" class="nav-link">About us</a></li>
						</ul>

						<ul class="nav navbar-nav navbar-right">
							<li class="nav-item"><a href="login.php" class="nav-link">Login</a></li>
							<li class="nav-item"><a href="register_form.php" class="nav-link">Register</a></li>
						</ul>					
					</div>
				</nav>
			-->


			<?php
			display_footer();
			?>