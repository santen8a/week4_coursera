<?php
require_once('../includes/site_functions.php');
session_start();
display_header('Login Page');
?>

<div class="row justify-content-center">
	<div class="col-md-6 col-sm-8">
		<?php if($msg = @$_GET['errMsg']){ display_alert_danger($msg); } ?>
		<?php display_login_form(); ?>
		<?php display_register_link(); ?>
	</div>

</div>

<?php
display_footer();
?>
