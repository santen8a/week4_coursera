<?php

require_once('../includes/site_functions.php');
session_start();

@$songID = $_GET['id'];


//if the page was parsed without the songID parameter, it will be redirected to index.php.
if(!$songID){
	header('Location: index.php');
}

display_header(get_song_name($songID));

//retreive song details and store it.
$song_details = get_song_details($songID);

//update view, increase it by 1 every time lyrics is viewed.
increment_views($song_details['songID'],$song_details);


?>

<div class="row" id="content_main">
	<div class="col-md-4">
		<?php display_nav_function(); ?>
		<?php display_soundCloud_box(); ?>
		<?php display_most_viewed_box(); ?>

	</div>

	<div class="col-md-8">
		<?php display_lyrics_body($song_details); ?>
	</div>


</div>
<!--MAIN ROW ENDS HERE-->

<?php

display_footer();



?>