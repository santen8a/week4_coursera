<!DOCTYPE html>
<html>
<head>
	<title>Wind Load</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<script type="text/javascript">
		function genk1(){
			var basicSpeed =document.getElementById('vb');
			var basicSpeedValue = parseInt(basicSpeed.options[basicSpeed.selectedIndex].value);

			var designLife =document.getElementById('dl');
			var designLifeValue = parseInt(designLife.options[designLife.selectedIndex].value);
			
			var k1 = "Undefined";
			switch(designLifeValue){
				case 50:
				k1=1;
				break;

				case 5:
				switch(basicSpeedValue){
					case 33:
					k1=0.82;
					break;

					case 39:
					k1=0.76;
					break;

					case 44:
					k1=0.73;
					break;

					case 47:
					k1=0.71;
					break;

					case 50:
					k1=0.70;
					break;

					case 55:
					k1=0.67;
					break;

					default:
					k1="Undefined";
				}
				break;

				case 25:
				switch(basicSpeedValue){
					case 33:
					k1=0.94      ;
					break;

					case 39:
					k1=0.92;
					break;

					case 44:
					k1=0.91;
					break;

					case 47:
					k1=0.90;
					break;

					case 50:
					k1=0.90;
					break;

					case 55:
					k1=0.89;
					break;
					
					default:
					k1="Undefined";
				}
				break;

				case 100:
				switch(basicSpeedValue){
					case 33:
					k1=1.05;
					break;

					case 39:
					k1=1.06;
					break;

					case 44:
					k1=1.07;
					break;

					case 47:
					k1=1.07;
					break;

					case 50:
					k1=1.08;
					break;

					case 55:
					k1=1.08;
					break;
					
					default:
					k1="Undefined";
				}
				break;
				default:
				k1="Undefined-A";
			}


			document.getElementById('k1').value = k1;
		}
	</script>
</head>
<body>

	<table class="table table-dark">
		<tr><td>a</td></tr>
	</table>

	<div class="container">
		<div class="row">
			<h1>Design Speed</h1>
			<p>Vz = Vb k1 k2 k3 k4</p>

			<form name=input1 method="POST" action="WindLoad.php">


				<table class="table table-hover">
					<tr>
						<th>Parameter</th><th>Value</th><th>Value</th><th>Note</th>
					</tr>
					<tr>	
						<td>Vb</td><td>
							<div class="form-group">
								<select id="vb" name="vb" class="form-control" onchange="genk1();">
									<option value="33">33</option>
									<option value="39">39</option>
									<option value="44">44</option>
									<option value="47">47</option>
									<option value="50">50</option>
									<option value="55">55</option>
								</select>
							</div>
						</td>
						<td><p>m/s</p></td>
						<td>Depends on Location <a href="img/image--002.jpg" target="_blank">Click here for map</a></td>
					</tr>

					<tr>	
						<td>k1</td><td>
							<div class="form-group">
								<select id ="dl" name="dl" class="form-control" onchange="genk1();">
									<option value = "">Select design life (years)</option>
									<option value="50" title="All general buildings and structures.">50 years</option>
									<option value="5" title="Temporary sheds, structures such as those used during construction operations (for example, formwork and false work),structures during construction stages, and boundary walls">5 years </option>
									<option value="25" title="Buildings and structures presenting a low degree of hazard to life and property in the event of failure, such as isolated towers in wooded areas, farm buildings other than residential buildings, etc.">25 years 
									</option>
									<option value="100" title="Important buildings and structures such as hospitals, communication buildings, towers and power plant structures">100 years</option>
								</select>
							</div>
							<div class="form-group">
								<input class="form-control" type="text" id="k1" name="k1" placeholder="Auto-Generated" readonly="true">
							</div>
						</td>
						<td><p>-</p></td>
						<td>probability factor (risk coefficient)</td>
					</tr>
					<tr>
						<td>k2</td>
						<td><div class="form-group">
							<select id ="tc" name="tc" class="form-control" onchange="genk2();">
								<option value = "">Select Terrain Caregory</option>
								<option value="50" title="Exposed open terrain
								with a few or no obstructions and in which the average height of any object	surrounding the structure is less than 1.5 m.">Terrain Caregory 1</option>
								<option value="5" title=" Open terrain with wellscattered obstructions having height. generally between 1.5 and 10 m. ">Terrain Caregory 2</option>
								<option value="25" title="Terrain with numerous closely spaced obstructions having the size of building-structures up to 10 m in height with or without a few isolated tall structures.">Terrain Caregory 3 
								</option>
								<option value="100" title=" Terrain with numerous large high closely spaced obstructions.">Terrain Caregory 4</option>
							</select>
						</div>
						<div class="form-group">
							<input class="form-control" type="text" id="k1" name="k1" placeholder="Auto-Generated" readonly="true">
						</div></td>
						<td></td>
						<td> terrain roughness and height factor </td>
					</tr>
				</table>
			</form>
		</div>

	</div>

</body>
</html>